#include <stdlib.h>

typedef struct noeud_t
{
	int data;
	struct noeud_t* suivant;
} noeud;

/* creer_noeud: renvoie un pointeur vers un nouveau noeud avec la valeur passée en paramètre */
noeud* creer_noeud(int data)
{
	noeud* nouveau_noeud = malloc(sizeof(noeud));
	nouveau_noeud->data = data;
	nouveau_noeud->suivant = NULL;
	return nouveau_noeud;
}

/* inserer_noeud: insère un noeud avec la valeur passée en paramètre */
noeud* inserer_noeud(int data, noeud* tete)
{
	noeud* nouvelle_tete = creer_noeud(data);
	nouvelle_tete->suivant = tete;
	return nouvelle_tete;
}

/* supprimer_tete: supprimer le noeud de tête */
noeud* supprimer_tete(noeud* tete)
{
	if ( tete == NULL )
		return NULL;
	else
	{
		noeud* nouvelle_tete = tete->suivant;
		free(tete);
		return nouvelle_tete;
	}
}
